{ config, pkgs, ... }:

{

  # enable system76 hardware 
  #  hardware.system76.enableAll = true;
  # not using nixos on my system 76 laptop as 2024-12-24

  # for framework laptops
  # from https://grahamc.com/blog/nixos-on-framework/
  # boot.kernelPackages = pkgs.linuxPackages_latest;
  # - for WiFi support, already part of main configuration.nix file
  #  services.fprintd.enable = true; 
  # for fingerprint support

  # Better scheduling for CPU cycles - thanks System76!!!
  services.system76-scheduler.settings.cfsProfiles.enable = true;

  # Enable TLP (better than gnomes internal power manager)
  services.tlp = {
    enable = true;
    settings = {
      CPU_BOOST_ON_AC = 1;
      CPU_BOOST_ON_BAT = 0;
      CPU_SCALING_GOVERNOR_ON_AC = "performance";
      CPU_SCALING_GOVERNOR_ON_BAT = "powersave";
    };
  };

  # Disable GNOMEs power management
  services.power-profiles-daemon.enable = false;

  # Enable powertop
  powerManagement.powertop.enable = true;

  # Enable thermald (only necessary if on Intel CPUs)
  services.thermald.enable = true;
 
  # enable laptop services
    services.acpid.enable = true;
    services.fwupd.enable = true;


}
