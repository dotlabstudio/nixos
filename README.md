# NixOS KDE Plasma Wayland for a Studio Laptop

This my configuration of NixOS with KDE Plasma Wayland for a Studio Laptop.

Alot of the code was borrowed from Hervy Qurrotul Ainur Rozi - see his github page at :

```
https://github.com/hervyqa/dotfire
```

and from

```
https://github.com/TechsupportOnHold
```

I wanted to clone hervyqa setup, but nixos-rebuild switch would always ended up with errors for me.
Yes, I'm a noob with NixOS, this repo is my newbie journey into the NixOS.

So, I built my nix configuration from the ground up. I prefer a flat structure for my configs instead of nested modules.

Install nixos with the graphical installer

Then git clone this repo with:

```
nix-shell -p git vim 

git clone https://gitlab.com/dotlabstudio/nixos.git
```

# Make backup copies of the existing nix config files with:

```
sudo cp /etc/nixos/configuration.nix /etc/nixos/defaultv0.configuration.nix.original

sudo cp /etc/nixos/hardware-configuration.nix /etc/nixos/defaultv0.hardware-configuration.nix.original
```

Then edit the new configuration.nix to suit the new hardware - copy the bootloader section and the luks section from the original configuration.nix to the bootloader.nix (bootloader.nix.vm in this repo) file. 

The locale.nix, users.nix, hostname.nix, laptop.nix, autoLogin.nix, wheel.nix and fnvidia.nix should be change to suit your needs.

The software.nix and fonts.nix files should be edit often to suit your needs.

Hardware or machine specific nix modules:

- bootloader.nix
- laptop.nix
- hostname.nix
- wheel.nix
- fnvidia.nix


# Change username with vim

```
:%s/changeme/yourusername/g
```

in normal mode.


The letter s stands for substitute.
The keyword % will target the entire file instead of the current line.
The flag g means “global”: more than one occurrence is targeted. Without it, only the first occurrence in the file (or the in line) would be replaced.

# Change username in all the nix files with vim

```
cd nixos
vim
:arg *.nix
:argdo %s/changeme/yourusername/ge | update
:wq
```
the e flag just prevents vim to display an error message when the search pattern can not be found in a file.

## Copy over new configs to /etc/nixos directory

```
sudo cp -r ~/nixos/*.nix /etc/nixos
```

Lastly, run:

```
sudo nix-channel --update
```

and then

```
sudo nixos-rebuild switch --upgrade
```

if there are no errrors, reboot.

# Future nixos rebuilds

To run future or subsquent nixo-os rebuilds, use the automated test script:

copy the automated script to the home directory

```
cp ~/nixos/testSubRuns.sh ~/.
```

then run the test script:
```
./testSubRuns.sh
```

# Nixos rolling

```
sudo nix-channel --list

sudo nix-channel --add https://channels.nixos.org/nixos-unstable nixos

sudo nixos-rebuild switch --upgrade
```

from https://nixos.org/manual/nixos/stable/#sec-upgrading


# Desktop Layout with Save Desktop flatpak

Work out how to declare my general kde plasma setup for :

top panel

konsole profile settings

etc.

without using home manager.

The best solution at the moment is save desktop from

```
https://flathub.org/en-GB/apps/io.github.vikdevelop.SaveDesktop
```

and

```
https://github.com/vikdevelop/SaveDesktop
```

To install save desktop after enabling flatpak:
```
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

flatpak install --user flathub io.github.vikdevelop.SaveDesktop
```

# Quad9 DNS

```
https://www.quad9.net/
```

for the browsers:
```
https://dns.quad9.net/dns-query

```

To check that quad9 is setup:
```
https://on.quad9.net 
```

# Setup new ssh pair keys for a new machine and copy public key to a server

To generate a new ssh pair keys on on a new machine:

```
ssh-keygen -t ed25519 -a 100
```

To copy public ssh keys to a server
```
ssh-copy-id -i ~/.ssh/id_ed25519.pub $USER@<serverIPorName>
```

# Tealdeer Setup

```
tldr  --update
```

# Atuin Setup

```
atuin mport auto
```

# Distrobox
To use command line packages for one off time, use distrobox.

To create a tumbleweed distrobox:
```
distrobox create --name pkg --image registry.opensuse.org/opensuse/distrobox-packaging:latest 
```

To update all containers
```
distrobox-upgrade --all
```
or
```
distrobox upgrade -a
```

# Use the nix-shell

Then just run:
```
nix-shell
```
Or, to be more explicit:
```
nix-shell shell.nix
```

nix run is a replacement for nix-shell -p, not for nix-shell in general.

nix run is a little easier to use for doing one-shot commands due to how it takes the command and arguments (as separate args to itself, whereas nix-shell -p requires you to cram them into a single argument). For example, with nix run:

```
nix run nixpkgs.hello -c hello --greeting 'Hi everybody!'
```
Hi everybody!
versus

```
nix-shell -p hello --run "hello --greeting 'Hi everybody!'"
```
Hi everybody!

# Adding a second drive to a NixOS system

create and format drive with kde partition manager or parted or some gui partition manager first


check drives with
```
lsblk
```

create a sensible mount first, for example 

```
sudo mkdir -p /secondDrive
```

change owner permissions on the mount point 

```
sudo chown -R $USER: users /secondDrive
```

manually mount the new drive to the mount point

```
sudo mount /dev/sdb1 /secondDrive
```

The command syntax for mounting the partition on your USB drive by device name:

sudo mount [DEVICE NAME] [MOUNT POINT]



generate a new hardware.nix file with 


```
sudo nixos-generate-config
```




