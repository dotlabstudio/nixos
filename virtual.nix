{
  config,
  pkgs,
  lib,
  ...
}: {
      
  # virtual machines
    # Manage the virtualisation services
    #  virtualisation = {
    #           libvirtd = {
    #           enable = true;
    #    qemu = {
    #    swtpm.enable = true;
    #    ovmf.enable = true;
    #    ovmf.packages = [ pkgs.OVMFFull.fd ];
    #  };
    # };
    # spiceUSBRedirection.enable = true;
  # };
  # not using virt manager now - switched to proxmox 
    
  # enable spice services for guest os and for proxmox 
    services.spice-vdagentd.enable = true;
    services.spice-webdavd.enable = true;
    services.qemuGuest.enable = true;

  # enable virt manager 
  #  programs.virt-manager.enable = true;

  # enable podman to be used with distrobox
  #  virtualisation.podman = {
  #         enable = true;
  #
  #         dockerCompat = true;
  #  };
  # not using anymore

  # vmware
  # remember to add open-vm-tools on the guest machines
  #  virtualisation.vmware.host.enable = true;
  # proprietary garbage with licensing keys 


  # virtualbox is the worst for virtual machines and why it's last
  # virtualbox and virtualbox extensions
  #  virtualisation.virtualbox.host.enable = true;
  #  virtualisation.virtualbox.host.enableExtensionPack = true;

  # virt manager is the king for me now
  # proxmox is now the new king for me as 2024-12-24
    
  
}
