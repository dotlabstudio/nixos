{ config, pkgs, lib, ... }:

{ 
    
    # create a strong and complex password beforehand 

    # don't use this option in a production machine, 
    
    # but it's a good option in virtual machines and remote servers
    # once public keys are setup

      security.sudo.wheelNeedsPassword = false;

    # the default is true

    # use false if on a virtual machine or testing machine or
    # on proxmox 


}
