# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./bootloader.nix
      ./hardware-acceleration.nix
      ./laptop.nix
      ./hostname.nix
      ./locale.nix
      ./users.nix
      ./autoLogin.nix
      ./software.nix
      ./fonts.nix
      ./virtual.nix
      ./firefox.nix
      ./wheel.nix
      ./sshServer.nix
      ./fnvidia.nix

    ];
  
  # hardware specific section - move to bootloader.nix 

  # copy the original hardware specific section to bootloader.nix file

  # move hostname to hostname.nix file

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Australia/Melbourne";

  # locale section move to locale.nix
  # Select internationalisation properties.

  # tty console settings
   console = {
    packages=[ pkgs.terminus_font ];
    font="${pkgs.terminus_font}/share/consolefonts/ter-i22b.psf.gz";
    useXkbConfig = true; # use xkbOptions in tty.
  };
  
  # Enable the X11 windowing system.
  # still needed for wayland plasma 6
    services.xserver.enable = true;

  # system76 laptops - moved to laptop.nix
 
  # Enable the KDE Plasma Desktop Environment.
    services.desktopManager.plasma6.enable = true; 
    services.displayManager.sddm.wayland.enable = true;
  
  # apply gtk themes in wayland
    programs.dconf.enable = true;

  # hint to apps to use wayland features when available
    environment.sessionVariables = {
           MOZ_ENABLE_WAYLAND = "1";
           NIXOS_OZONE_WL = "1";
    };
  
  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "au";
    variant = "";
  };

  # Enable CUPS to print documents.
  # enable internet wireless printing and scanning
    services.printing.enable = true;
    services.avahi.enable = true;
    services.avahi.nssmdns4 = true;
    
  # for a WiFi printer
    services.avahi.openFirewall = true;

    services.printing.drivers = [ pkgs.hplip
                                  pkgs.hplipWithPlugin
                                  pkgs.samsung-unified-linux-driver
                                  pkgs.splix pkgs.brlaser

                                ];

  # enable gui app to manage cups printer
    programs.system-config-printer.enable = true;

  # enable scanner support
    hardware.sane.enable = true;
    hardware.sane.extraBackends = [ pkgs.hplipWithPlugin
                                    pkgs.sane-airscan

                                  ];

  # scanner is also an usb scanner
    services.ipp-usb.enable=true;

  # Enable sound with pipewire.
  # sound.enable = true;
  # sound not needed now 
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
      jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
   services.libinput.enable = true;
  
  # user section move to users.nix with default shell being fish
  # Define a user account. Don't forget to set a password with ‘passwd’.
  programs.fish.enable = true;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Enable flatpaks
  services.flatpak.enable = true;
  services.dbus.enable = true;

  # Enable xdg portals
    xdg = {
        portal = {
          enable = true;
          lxqt = {
             enable = false;
             styles = with pkgs;
                 with kdePackages;
                    [
                     qtstyleplugin-kvantum
                     breeze 
                    ];
      };
      wlr = {
        enable = false;
      };
      extraPortals = with pkgs; [
        xdg-desktop-portal-kde
        xdg-desktop-portal-gtk
        xdg-desktop-portal-wlr
        ];
      };
    };

  # Remember to setup the flathub remote
  # flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
  # flatpak install --user --noninteractive --assumeyes flathub org.standardnotes.standardnotes
  # for example 

  # move list packages to software.nix
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # overlays are in the programs directory

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # enable tailscale
    services.tailscale.enable = true;
    
  # hardware wallets
    services.trezord.enable = true;

  # enable faster reboots
    systemd.extraConfig = "DefaultTimeoutStopSec=10s";

  # security improvements
    services.sysstat.enable = true;

  # enable auditd
    security.auditd.enable = true;
    security.audit.enable = true;
    security.audit.rules = [
        "-a exit,always -F arch=b64 -S execve"
    ];

  # enable out of memory stuff
    services.earlyoom = {
      enable = true;
      freeSwapThreshold = 5;
      freeMemThreshold = 5;
      extraArgs = [
          "-g" "--avoid '^(X|plasma.*|konsole|kwin)$'"
          "--prefer '^(electron|libreoffice|gimp)$'"
      ];
    };

  # enable laptop services - moved to laptop.nix
 
  # exclude some kde apps
    environment.plasma6.excludePackages = with pkgs.kdePackages; [
       elisa 
       plasma-browser-integration
  
    ];


  # to access removeable drives
    services.udisks2.enable = true;
    boot.supportedFilesystems = [ "ntfs" ];

  # more stuff for removeable drives
    services.udev.enable = true;
    services.devmon.enable = true;

  # moved sudo password to wheel.nix 

  # enable GVFS to work multiple filesystems
    services.gvfs.enable = true;

  # enable envfs 
    services.envfs.enable = true;
    # Fuse filesystem that returns symlinks to executables based on the PATH of the requesting process. 
    # This is useful to execute shebangs on NixOS that assume hard coded locations in locations like /bin or /usr/bin etc.

  # option to enable Bluetooth
    hardware.bluetooth.enable = true;

  #  enable kde partition manager
     programs.partition-manager.enable = true;

  #  enable ssd trim 
     services.fstrim.enable = true;

  #  setup wireshark
     programs.wireshark.enable = true;

  #  use the latest kernel
  #  need to use the latest kernel for framework, system76 and other laptops 
     boot.kernelPackages = pkgs.linuxPackages_latest;  

  # kernel stuff for obs studio 
    boot.kernelModules = [ "v4l2loopback" "snd-aloop" ];

    boot.extraModulePackages = with config.boot.kernelPackages; [
    v4l2loopback
                                ];

    boot.extraModprobeConfig =
         ''
           options v4l2loopback nr_devices=2 exclusive_caps=1,1 video_nr=0,1 card_label=v4l2lo0,v4l2lo1
         '';    

  
  # enable antivirus clamav and
  # keep the signatures' database updated
    services.clamav.daemon.enable = true;
    services.clamav.updater.enable = true;

  # enable locate and mlocate services
    services.locate = {
              enable = true;
              interval = "hourly";
                      };

  # enable nix-ld to run unpatched dynamic binaries on nixos 
  #  programs.nix-ld.enable = true;

  # nixos script to show Summary of Changes After nixos-rebuild
  # reference : https://github.com/mcdonc/.nixconfig/blob/master/videos/tipsntricks/script.rst
  # original source: https://github.com/luishfonseca/dotfiles/blob/main/modules/upgrade-diff.nix
  # very useful script 
    system.activationScripts.diff = {
        supportsDryActivation = true;
        text = ''
        ${pkgs.nvd}/bin/nvd --nix-bin-dir=${pkgs.nix}/bin diff \
            /run/current-system "$systemConfig"
       '';
    };

  # enable k3b burner software 
    programs.k3b.enable = true;

  # flashing android phones to install CalyxOS
    services.udev.packages = [
                                pkgs.android-udev-rules

                             ];
                             
  # cleaning the nix store

  # Optimise the store 
    nix.settings.auto-optimise-store = true;

  # Automatic Garbage Collection
    nix.gc = {
                automatic = true;
                dates = "weekly";
                options = "--delete-older-than 7d";
        };

  # automatic upgrades
    system.autoUpgrade.enable = true;
    system.autoUpgrade.allowReboot = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  system.copySystemConfiguration = true;

  # virtual machines move to virtual.nix 
  
  # moved openssh server to sshServer.nix 

  # enable firewall and block all ports
    networking.firewall.enable = true;
    networking.firewall.allowedTCPPorts = [];
    networking.firewall.allowedUDPPorts = [];

  # Tell the firewall to implicitly trust packets routed over Tailscale:
    networking.firewall.trustedInterfaces = [ "tailscale0" ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.11"; # Did you read the comment?

}
