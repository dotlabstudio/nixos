{ config, pkgs, lib, ... }:

{ 
    
    # ssh server configs 
    # enabled by default for proxmox and lynis now 

    # Enable the OpenSSH daemon.
    #  services.openssh.enable = true;

    services.openssh = {
      enable = true;
      # enable = false;
      # disable ssh server by default now 
      settings.LogLevel = "VERBOSE";
      banner = "

#################################################################
#                   _    _           _   _                      #
#                  / \  | | ___ _ __| |_| |                     #
#                 / _ \ | |/ _ \ '__| __| |                     #
#                / ___ \| |  __/ |  | |_|_|                     #
#               /_/   \_\_|\___|_|   \__(_)                     #
#                                                               #
#  You are entering into a secured area! Your IP, Login Time,   #
#   Username has been noted and has been sent to the server     #
#                       administrator!                          #
#   This service is restricted to authorized users only. All    #
#            activities on this system are logged.              #
#  Unauthorized access will be fully investigated and reported  #
#        to the appropriate law enforcement agencies.           #
#################################################################

Welcome Back Legend !

NixOS home lab

";
        extraConfig = "

        AllowTcpForwarding no

        ClientAliveCountMax 2

        Compression no

        MaxAuthTries 3

        MaxSessions 2

        TCPKeepAlive no
        
        AllowAgentForwarding no

        ";

};

  
}
