{config, pkgs,lib, ...}: 

{
  
  environment = {
    systemPackages = with pkgs; [
     
     # base packages
     at-spi2-atk
     #at-spi2-core is an important file to include for dbus stuff

     # for running app images on nixos
     appimage-run

     aria
     # A lightweight, multi-protocol, multi-source, command-line download utility
     # aria2 - yt-dlp uses it 

     curl

     exfat
     exfatprogs
     # exfat stuff needed for removable drives

     ffmpeg
     ffmpegthumbnailer
     kdePackages.ffmpegthumbs

     glxinfo
     git
     gnupg
     gvfs
     htop
     
     hydra-check
     #check hydra for the build status of a package
     #hyra-check appname

     libva
     # hardware Video Acceleration

     lolcat
     mediainfo 
     minisign
     moreutils
     #neofetch 
     # has been sunset
     fastfetch
     nerdfonts
     nfstrace
     nfs-utils

     # nixos stuff 
     nix-tree
     nixos-option
     nix-du
     graphviz
     nix-index
     # from https://github.com/mcdonc/.nixconfig/blob/master/videos/tipsntricks/script.rst

     ntfs3g
     pinentry
     procps
     rsync
     shared-mime-info
     signify
     stdenv
     tmux
     usbutils

     vim # Do not forget to add an editor to edit configuration.nix! 
     #The Nano editor is also installed by default.

     wget

     yt-dlp

     # rust cli tools
     bat 

     du-dust
     # du + rust = dust. Like du but more intuitive

     eza

     fortune-kind
     # A kinder, curated fortune, written in rust

     fzy

     helix
     # modern vim-like text editor written in rust 

     lsd 
     ouch
     procs
     ripgrep
     sd
     skim
     tealdeer

     # spellcheck dictionaries
     aspell
     aspellDicts.en
     aspellDicts.en-computers
     aspellDicts.en-science
     hunspell
     hunspellDicts.en_AU-large 
     hunspellDicts.en_GB-large

     # archive tools
     atool
     bzip2
     gzip
     libarchive
     lz4
     lzip
     lzo
     lzop
     p7zip
     rzip
     unrar
     unzip

     #xz
     # secuirty alert for xz as easter sunday on 2024-03-31
     # nixos says they will fix it in 10 days 

     zip
     zstd

     # test os security
     lynis
     chkrootkit
     
     # hardware tools
     amdvlk
     lshw
     #rocm-opencl-icd
     # not needed now 
     pciutils
     nvme-cli

     # usb removable drives Utilities
     psmisc
     # A set of small useful utilities that use the proc filesystem (such as fuser, killall and pstree)

     #ipscan
     # same as angry ip scanner
     # use old gtk version - does not load properly on plasma

     # Invisible Internet Project (I2P) 
     i2p

     # debugging nixos rebuilds
     nvd
     #Nix/NixOS package version diff tool
     cntr
     #A container debugging tool based on FUSE

     pandoc
     # Conversion between documentation formats

     pdfcpu
     # A PDF processor written in Go

     zoxide
     # A fast cd command that learns your habits

     # for plasma 6
       kdePackages.kcmutils
     # Utilities for interacting with KCModules

       kdePackages.mlt
       # needed for kdenlive

     # gui apps
     #firefox
     # has it's own nix module

     # Development
     # kdePackages.kate
     # be by default in plasma 6

     meld

     # mind mapping app
     #minder
     # does not load properly under wayland 

     # Education
     #  kdePackages.marble
     # marked as a broken package as 2024-03-27
     # still broken as 2024-04-06

     # Geographic Information System
     #qgis

     # Games
     wesnoth
     gnome-chess
     
     kdePackages.knights

     pokerth

     #superTux
     #superTuxKart
     #xonotic

     # need a chess engine
     stockfish

     # Graphics
     blender
     digikam
     darktable
     gImageReader
     gimp
     inkscape

     kdePackages.kcolorchooser

     krita

     # kde paint app
     kdePackages.kolourpaint

     # scanner app
     #gnome.simple-scan

     kdePackages.skanlite

     # Internet
     brave
     # does not like the latest kernel if is not hardern

     #dialect
     # works better as flatpak

     filezilla

     #floorp
     # upcoming version 12 will go closed source

     lagrange

     #An open source cross-platform alternative to AirDrop
     localsend
     
     # xmr stuff 
     monero-gui
     monero-cli
     xmrig
     p2pool
     
     #exodus
     # maybe better off with the flatpak version

     mullvad-browser

     tor
     #onionshare-gui
     #onionshare

     #qbittorrent

     # virtual remote desktops 
     #remmina
     
     # secure messagers
     signal-desktop
     # got a new iphone that works with signal again 

     tor-browser-bundle-bin
     # tor stuff works well on nixos now

     # network scanners 
     nmap
     wireshark

     # Multimedia
     #ardour
     # too advanced for me at the moment

     # screen casting
     #gnome-network-displays
     # does not work well

     handbrake
     # rebuild errors as 2024-03-26
     # uses ffmpeg-full-5.1.2

     # mpv kde front end
     haruna

     # dvd drives - yes - I still use them
     libdvdcss

     kdePackages.k3b

     cdrtools
     # Highly portable CD/DVD/BluRay command line recording software

     cdrkit
     # Portable command-line CD/DVD recorder software, mostly compatible with cdrtools
     # Cdrkit is a suite of programs for recording CDs and DVDs, blanking CD-RW media, 
     # creating ISO-9660 filesystem images, extracting audio CD data, and more. 
     # The programs included in the cdrkit package were originally derived from several sources,
     # most notably mkisofs by Eric Youngdale and others,
     # cdda2wav by Heiko Eissfeldt, and cdrecord by Jörg Schilling. 
     # However, cdrkit is not affiliated with any of these authors; it is now an independent project.


     #kdePackages.kamoso
     # rebuild errors as 2024-03-21
     # marked as a broken package as 2024-03-26
     # still broken as 2024-04-06

     #For `nixos-rebuild` you can set
     #    { nixpkgs.config.allowBroken = true; }
     #  in configuration.nix to override this.


     kdePackages.kdenlive

     # apps for kdenlive
     #mediainfo
     glaxnimate

     #picard
     # need a better tool uses ai to auto detect and organise meta data on book and music collections etc 

     #obs-studio packages stuff 

     # obs studio wiki setup requires the same libraries as livecaptions 
     # -it's not rebuilding as 2024-02-11
     # obs-backgroundremoval not rebuilding properly 
     (pkgs.wrapOBS {
               plugins = with pkgs.obs-studio-plugins; [
                         wlrobs
                         obs-backgroundremoval
                         obs-pipewire-audio-capture
                         ];
                   })
     
     qpwgraph

     strawberry-qt6

     #tenacity
     # audacity is still better 

     vlc
     kdePackages.phonon-vlc

     # Office

     # more translate apps
     #crow-translate
     # works 

     # ocr app
     #gnome-frog

     #libreoffice-fresh
     # does not well on wayland or plasma 6

     libreoffice-qt-fresh

     #onlyoffice-bin
     #onlyoffice-bin_latest
     # onlyoffice uses third parties like aws and google for some stuff
     # also it's not available on FreeBSD too 

     pdfarranger

     #standardnotes
     # flatpak version works better
     # stick to the flatpak
     # hopefully one day the nixos native package works better than the flatpak version 
     # don't use standardnotes much now after switching to notesnook 
     # also package is still broken under nixos 

     notesnook
     # A fully open source & end-to-end encrypted note taking alternative to Evernote.

     xournalpp

     # Settings
     # Colorwall - only available as a flatpak 

     # gui app to manage cups printers
     system-config-printer

     # virtual machines stuff 
     #virt-manager
     # moved to virtual.nix now
     # switched tp proxmox now - not using virt manager 
     
     #libverto
     #ventoy-full
     #win-virtio
     #virt-viewer
     #spice
     #spice-gtk
     #spice-protocol
     #win-spice
     #adwaita-icon-theme

     # file sharing for virt manager
     #virtiofsd

     # under virtual guest machines, add
     # <binary path='/run/current-system/sw/bin/virtiofsd'/>
     # below the driver under the xml

     # Raspberry Pi Imaging Utility
     # replaces etcher for me 
     rpi-imager


     # Utilities
     #cryptomator
     # broken package as 2024-12-24
     
     kdePackages.kcalc

     # screen recording
     kooha

     # Shazamn like app
     #mousai

     livecaptions
     
     nextcloud-client

     # kalpa apps
     #distrobox
     #podman
     #podman-compose
     # not using distrobox or docker on the laptop

     #podman-desktop
     # not really needed 

     trezor-suite
     
     # trezor ssh stuff
     (python3.withPackages(ps: with ps; [ trezor_agent wheel]))
    
     
     #vorta
     # not really needed unless I get more money to work with borgbase

     # Lost & Found
     # Speech Note only available as flatpak at the moment
     # not really used at the moment 

     # extra stuff
     #davinci-resolve
     # needs the unstable channel and still a pain to setup under linux
     # needs to a flatpak like it's on the mac app store
     # runs best on a mac

     # screen recorder using the gpu 
     #gpu-screen-recorder
     # A screen recorder that has minimal impact on system performance by recording a window using the GPU only
     # rebuild and website access errors

     # rust based terminal - better macos terminal so far 
     # only macos is well supported so far 
     #warp-terminal
     #
     # also needs
     # { nixpkgs.config.allowUnsupportedSystem = true; }
     #        in configuration.nix to override this.

     # open source alternative to the warp terminal is wave terminal - available as an appimage at the moment

     # creating and managing digital music librariesin linux

     # cd audio ripping
       #sound-juicer
       freac
       #asunder
       #rhythmbox - flatpak version could not detect the burner but freac did 

     # audio editor and converter
       audacity

     # tag tools 
       #eartag
       tagger
       #kid3-qt

     # normalise levels
       mp3gain

     # software to control fans on amd chips
     # Linux AMDGPU Controller
     #  lact
     # needs lactd - system daemon to be enabled - nixos does not have that package as yet
       
     # more apps in the future
     


    ];
  };

  
}
