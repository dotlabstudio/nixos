{ config, pkgs, ... }:
{
  
  networking.hostName = "nixos"; # Define your hostname. Change this for different machines
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  #networking.hostName = "elysium";
  # Elysium (Elysian Fields) 	Mythological 	
  # In Greek mythology, the final resting place of the souls of the heroic and the virtuous. 

  #networking.hostName = "olympos";
  # Mount Olympus 	Mythological 	
  # "Olympos" was the name of the home of the Twelve Olympian gods of the ancient Greek world.
  
  #networking.hostName = "asgard";
  # Asgard 	
  # The high placed city of the gods, built by Odin, chief god of the Norse pantheon. 

  #networking.hostName = "valhalla";
  # Valhalla 
  #	(from Old Norse Valhöll "hall of the slain")
  # is a majestic, enormous hall located in Asgard, ruled over by the god Odin. 


  #networking.hostName = "delphi"; 
  # Delphi

  #networking.hostName = "epoch"; 
  # Last epoch 

  #networking.hostName = ""; 
  # blank hostname 


}
