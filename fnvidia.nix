{ config, pkgs, lib, ... }:

{ 
  # use this only for nvidia machines
  # by default everything is masked here 

  # enable cuda cache 
    #nix.settings = {
    #                 substituters = [
    #                                 "https://cuda-maintainers.cachix.org"
    #                                ];
    #                 trusted-public-keys = [
    #                                        "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="
    #                                       ];
    #               };

  # Make sure opengl is enabled

  #hardware.opengl = {
  #  enable = true;
  #  driSupport = true;
  #  driSupport32Bit = true;
  #};

  # NVIDIA drivers are unfree.

  #nixpkgs.config.allowUnfreePredicate = pkg:
  #  builtins.elem (lib.getName pkg) [
  #    "nvidia-x11"
  #  ];

  # Tell Xorg to use the nvidia driver

  #services.xserver.videoDrivers = ["nvidia"];

  #hardware.nvidia = {

    # Modesetting is needed for most wayland compositors
    
    #modesetting.enable = true;

    # Use the open source version of the kernel module
    
    #open = true;

    # Enable the nvidia settings menu
    
    #nvidiaSettings = true;

    # Optionally, you may need to select the appropriate driver version for your specific GPU.

    #package = config.boot.kernelPackages.nvidiaPackages.stable;
  #};
  
}
