{ config, pkgs, lib, ... }:

{ 

         # Enable automatic login for the user. Please change the user name to your own.
         services.displayManager.autoLogin.enable = true;
         services.displayManager.autoLogin.user = "changeme";

}
