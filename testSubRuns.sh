#!/usr/bin/env bash

# this script automates my test run for a new configuration.nix

rm -rf nixos

echo "git clone my nixos repo"
echo " "

git clone https://gitlab.com/dotlabstudio/nixos.git

echo "copy my core nix files to /etc/nixos on the system of this machine"
echo " "

sudo cp nixos/configuration.nix /etc/nixos

sudo cp nixos/software.nix /etc/nixos

sudo cp nixos/fonts.nix /etc/nixos

sudo cp nixos/virtual.nix /etc/nixos

#sudo cp nixos/laptop.nix /etc/nixos

sudo cp nixos/sshServer.nix /etc/nixos

sudo cp nixos/hardware-acceleration.nix /etc/nixos

echo "update nix channel before updating the system"
echo " "

sudo nix-channel --update

echo "rebuild the entire os again with the new configurations"
echo " "

sudo nixos-rebuild switch --upgrade-all


echo "Please reboot machine with the new configuration.nix if it compile without errors"

