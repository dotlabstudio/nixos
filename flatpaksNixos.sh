#!/usr/bin/env bash

echo "This script will apps better as flatpaks on a nixos system"

# install flatpaks from flathub
#https://flathub.org/home
#
#flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#  the above one installs flathub repos as a user
flatpak remote-add --user --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# Internet
#flatpak install --user --noninteractive --assumeyes flathub com.brave.Browser
# got issues with brave nixos when it's not harden
# fixed again

#flatpak install --user --noninteractive --assumeyes flathub org.cockpit_project.CockpitClient
# better off just using the web browser than the client

#flatpak install --user --noninteractive --assumeyes flathub app.drey.Dialect
# nixos apps complains about connections and dns problems

# flatpak install --user --noninteractive --assumeyes flathub org.mozilla.firefox
# turn off harden profiles in nixos - it works now

#flatpak install --user --noninteractive --assumeyes flathub org.getmonero.Monero

#flatpak install --user --noninteractive --assumeyes flathub org.onionshare.OnionShare
# nixos app don't appear in the menu and tor works better as flatpak

#flatpak install --user --noninteractive --assumeyes flathub com.github.micahflee.torbrowser-launcher

# Multimedia
#flatpak install --user --noninteractive --assumeyes flathub org.kde.kdenlive
#nixos app complains about glaximate even it's installed

#flatpak install --user --noninteractive --assumeyes org.videolan.VLC
# nixos vlc opens but crashes when it try to play a video
# vlc nixos package works again as 2024-03-21
# open file/directory on vlc does not work on plasma 6 at the moment
# can play files under dolphin

# Office
#flatpak install --user --noninteractive --assumeyes flathub org.onlyoffice.desktopeditors
# not working at the moment

flatpak install --user --noninteractive --assumeyes flathub org.standardnotes.standardnotes
#nixos app double click does not work out of the box

flatpak install --user --noninteractive --assumeyes flathub com.notesnook.Notesnook
# A fully open source & end-to-end encrypted note taking alternative to Evernote
# nixos package not the latest version and takes forever to manually update or sync
# flatpak is verified 

# Utilities
#flatpak install --user --noninteractive --assumeyes flathub com.ktechpit.colorwall
# uses old runtimes

#flatpak install --user --noninteractive --assumeyes flathub net.sapples.LiveCaptions
# nixos version not working or compiling propely as 2024-01-29
# nixos package working again as 2024-02-17

#flatpak install --user --noninteractive --assumeyes flathub com.nextcloud.desktopclient.nextcloud

#flatpak install --user --noninteractive --assumeyes flathub io.github.vikdevelop.SaveDesktop

#flatpak install --user --noninteractive --assumeyes flathub net.mkiol.SpeechNote

#flatpak install --user --noninteractive --assumeyes flathub io.github.onionware_github.onionmedia
# only works in nixos well and there use resume function or recovery after a crash. Better off using yt-dlp now.
# also dones not on opensuse or moccino os

#flatpak install --user --noninteractive --assumeyes flathub io.appflowy.AppFlowy
# appflowy on the mac is not signed and unable to run

#flatpak install --user --noninteractive --assumeyes flathub io.exodus.Exodus
# not using it at the moment but the nixos package needs to be run from the commandline

# screen casting
#flatpak install --user --noninteractive --assumeyes flathub org.gnome.NetworkDisplays

# another yt-dlp frontend with a name change to parabolic
#flatpak install --user --noninteractive --assumeyes flathub org.nickvision.tubeconverter
# needs to be run from the commandline first on nixos for some reason - onion media x just works better for me

# more flatpaks to test


echo "Finished setting up flatpaks for nixos"
