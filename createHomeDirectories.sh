#!/usr/bin/env bash

echo " This script just create home directories for a fresh NixOS system"

cd ~

mkdir -p Documents

mkdir -p Downloads

mkdir -p Music

mkdir -p Pictures

mkdir -p Public

mkdir -p Videos/Jellyfin

mkdir -p Videos/videosDone

mkdir -p lab

mkdir -p ventoyBurned

mkdir -p ventoyISOs

mkdir -p zArchived

mkdir -p zOffline

echo "Finished setting home directories"
echo "Please copy download script to Downloads and Videos folders"
