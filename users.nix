{
  config,
  pkgs,
  lib,
  ...
}: let
  name = "changeme";
  fullname = "changeme";
in {
  users = {
    defaultUserShell = pkgs.fish;
    users = {
      ${name} = {
        isNormalUser = true;
        description = "${fullname}";
        uid = 1000;
        extraGroups = [
          "adbusers"
          "audio"
          "clamav"
          "corectrl"
          "disk"
          "docker"
          "kvm"
          "input"
          "jupyter"
          "libvirtd"
          "lp"
          "mlocate"
          "mongodb"
          "mysql"
          "network"
          "networkmanager"
          "plugdev"
          "postgres"
          "power"
          "scanner"
          "sound"
          "storage"
          "systemd-journal"
          "users"
          "vboxusers"
          "video"
          "wheel"
          "wireshark"
          
        ];
      };
    };
  };
}
